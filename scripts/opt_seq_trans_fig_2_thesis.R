
mypath = "~/assembly/gc-reads/"
gclist=c("mouse_SRR453174_1.gc.hist","mouse_SRR453174_2.gc.hist","mouse_heart_c3.gc.hist","mouse_heart_c10.gc.hist",
"cmulti_h80_transcripts.gc.hist","dosidicus_transcripts.gc.hist","harmothoe_transcripts.gc.hist","probusta3a_transcripts.gc.hist",
"sergestes_h80_transcripts.gc.hist","SNL_120217_SN141_0457_BD0M4JACXX_2_1.gc.hist","SNL_120217_SN141_0457_BD0M4JACXX_2_2.gc.hist",
"probusta_D356_3_a_1.gc.hist","probusta_D356_3_a_2.gc.hist","8952X7_120217_SN141_0457_BD0M4JACXX_3_1.gc.hist","8952X7_120217_SN141_0457_BD0M4JACXX_3_2.gc.hist",
"8952X8_120217_SN141_0457_BD0M4JACXX_3_1.gc.hist","8952X8_120217_SN141_0457_BD0M4JACXX_3_2.gc.hist",
"9110X11_120524_SN1117_0094_BD10B0ACXX_5_1.gc.hist","9110X11_120524_SN1117_0094_BD10B0ACXX_5_2.gc.hist",
"9110X8_120524_SN1117_0094_BD10B0ACXX_5_1.gc.hist","9110X8_120524_SN1117_0094_BD10B0ACXX_5_2.gc.hist",
"9110X13_120524_SN1117_0094_BD10B0ACXX_5_1.gc.hist","9110X13_120524_SN1117_0094_BD10B0ACXX_5_2.gc.hist",
"hormiphora_transcripts.gc.hist","mouse_trinity_h70_Trinity.gc.hist"
)

outfilename = "/home/dummy/Dropbox/thesis_final/osd_fig_2.pdf"

#pdf(file=outfilename, height = 6, width = 12)

alldata = integer()
count = 1
for (thefile in gclist){
	filename = paste(mypath,thefile,sep="")
	data = read.table(filename,FALSE,sep="\t")
	alldata = c(alldata,data[,2])
	count = count+1
}

# the number of files is here
datmap = matrix(alldata,ncol=25)

mycolors=rep(c("black"),8)
linewid = 2
cexsize = 2
line2 = 3
par(mfrow=c(2,4), mar=c(4.1,2.5,3.5,1))

prex1=seq(0,76)
x2=seq(0,100)
x1=prex1*100/76.0

#c3 mouse
prey1 = datmap[,1]+datmap[,2]
y1 = prey1[prey1>0]
ymax1 = max(y1)
y2 = datmap[,3]
ymax2 = max(y2)
z1 = prex1*y1
z2 = x2*y2
plot(x1,y1/ymax1,lty=1,col="black",type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y2/ymax2,lty=line2,col="black",lwd=linewid)
mtext("Mouse-C3",adj=0, cex=0.8, line=1, font=2)

#cmultidentata
y4 = datmap[,14]+datmap[,15]+datmap[,16]+datmap[,17]
ymax4 = max(y4)
y5 = datmap[,5]
ymax5 = max(y5)
plot(x2,y4/ymax4,lty=1,col=mycolors[7],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y5/ymax5,lty=line2,col=mycolors[7],lwd=linewid)
mtext("C.multidentata",adj=0, cex=0.8, line=1, font=4)

#sergestes
y6 = datmap[,10]+datmap[,11]
ymax6 = max(y6)
y7 = datmap[,9]
ymax7 = max(y7)
plot(x2,y6/ymax6,lty=1,col=mycolors[1],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y7/ymax7,lty=line2,col=mycolors[1],lwd=linewid)
mtext("S.similis",adj=0, cex=0.8, line=1, font=4)

#pleuromamma
y8 = datmap[,12]+datmap[,13]
ymax8 = max(y8)
y9 = datmap[,8]
ymax9 = max(y9)
plot(x2,y8/ymax8,lty=1,col=mycolors[5],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y9/ymax9,lty=line2,col=mycolors[5],lwd=linewid)
mtext("P.robusta",adj=0, cex=0.8, line=1, font=4)

#trinity mouse
y3 = datmap[,25]
ymax3 = max(y3)
plot(x1,y1/ymax1,lty=1,col="black",type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y3/ymax3,lty=line2,col="black",lwd=linewid)
mtext("Mouse-Trinity",adj=0, cex=0.8, line=1, font=2)

#dosidicus
y10 = datmap[,18]+datmap[,19]
ymax10 = max(y10)
y11 = datmap[,6]
ymax11 = max(y11)
plot(x2,y10/ymax10,lty=1,col=mycolors[3],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y11/ymax11,lty=line2,col=mycolors[3],lwd=linewid)
mtext("D.gigas",adj=0, cex=0.8, line=1, font=4)

#hormiphora
y12 = datmap[,22]+datmap[,23]
ymax12 = max(y12)
y13 = datmap[,24]
ymax13 = max(y13)
plot(x2,y12/ymax12,lty=1,col=mycolors[6],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y13/ymax13,lty=line2,col=mycolors[6],lwd=linewid)
mtext("H.californensis",adj=0, cex=0.8, line=1, font=4)

#harmothoe
y14 = datmap[,20]+datmap[,21]
ymax14 = max(y14)
y15 = datmap[,7]
ymax15 = max(y15)
plot(x2,y14/ymax14,lty=1,col=mycolors[2],type='l',lwd=linewid,frame.plot=FALSE, xlab="GC (%)", ylab="Fraction of Max")
lines(x2,y15/ymax15,lty=line2,col=mycolors[2],lwd=linewid)
mtext("H.imbricata",adj=0, cex=0.8, line=1, font=4)

#dev.off()
