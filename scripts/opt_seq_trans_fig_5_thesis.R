file1 = "/home/dummy/assembly/opt-seq-trans_data/Cmult_all_kog_results.txt"
file2 = "/home/dummy/assembly/opt-seq-trans_data/hormiphora_all_kog_results.txt"
file3 = "/home/dummy/assembly/opt-seq-trans_data/prob_all_kog_results.txt"
#file4 = "/home/dummy/assembly/opt-seq-trans_data/tomo_all_kog_results.txt"
file4 = "/home/dummy/assembly/opt-seq-trans_data/harmothoe_all_kog_results.txt"
file5 = "/home/dummy/assembly/opt-seq-trans_data/SNL_all_kog_results.txt"
file6 = "/home/dummy/assembly/opt-seq-trans_data/dosidicus_all_kog_results.txt"
file9 = "/home/dummy/assembly/opt-seq-trans_data/lampea_all_kog_results.txt"

outfilename = "/home/dummy/Dropbox/thesis_final/osd_fig_5.pdf"
#pdf(file=outfilename, height = 6, width = 12)

#png(file=outfilename, height = 600, width = 700)

#pull from kog_results files by:
# grep \\$ FILES | cut -d "$" -f 2 | cut -d " " -f 1 > output

paths = 9
k = 2^(seq(paths)-1)
m = t(matrix(k,paths,paths))+matrix(k,paths,paths)
mc = m*(((col(m)<row(m))*(-1)*2)+1)

data1 = read.table(file1, header=FALSE, sep="\n")
colcount1 = 10
datmap1 = matrix(data1[,1],nrow=248,ncol=colcount1)
statecounts1 = apply(datmap1,2,function(x) table(factor(x, levels=k)))
fullhits1 = (statecounts1['1',])
#fullhits1 = statecounts1['1',]+statecounts1['2',]
allhits1 = (colSums(statecounts1[1:8,]))
nohits1 = (statecounts1['256',])
summap1 = datmap1[,-1]+datmap1[,-ncol(datmap1)]
diffmap1 = t(diff(t(datmap1)))
fdmap1 = diffmap1
fdmap1[fdmap1<1]=(-1)
signmap1 = (-1)*sign(fdmap1)
modmap1 = summap1*signmap1
finalmap1 = matrix(append(datmap1[,1]+max(k),modmap1), nrow=248,ncol=colcount1)
mapcounts1 = apply(finalmap1,2,function(x) table(factor(x,levels=sort(mc))))
# transition probability table

data2 = read.table(file2, header=FALSE, sep="\n")
colcount2 = 6
datmap2 = matrix(data2[,1],nrow=248,ncol=colcount2)
#datmap2 = subset(datmap2, select=c(3,2,5,6,1,4))
statecounts2 = apply(datmap2,2,function(x) table(factor(x, levels=k)))
fullhits2 = (statecounts2['1',])
#fullhits2 = statecounts2['1',]+statecounts2['2',]
allhits2 = (colSums(statecounts2[1:8,]))
nohits2 = (statecounts2['256',])

data3 = read.table(file3, header=FALSE, sep="\n")
colcount3 = 9
datmap3 = matrix(data3[,1],nrow=248,ncol=colcount3)
datmap3 = subset(datmap3, select=c(3,2,5,8,1,4,6,7,9))
statecounts3 = apply(datmap3,2,function(x) table(factor(x, levels=k)))
fullhits3 = (statecounts3['1',])
#fullhits3 = statecounts3['1',]+statecounts3['2',]
allhits3 = (colSums(statecounts3[1:8,]))
nohits3 = (statecounts3['256',])

data4 = read.table(file4, header=FALSE, sep="\n")
colcount4 = 8
datmap4 = matrix(data4[,1],nrow=248,ncol=colcount4)
#datmap4 = subset(datmap4, select=c(2,3,4,5,1))
statecounts4 = apply(datmap4,2,function(x) table(factor(x, levels=k)))
#bp4 = barplot(statecounts4,col=rainbow(9,s=0.6))
#image(z=t(log(datmap1,2)+1),col=rainbow(9,s=0.8,start=0.1,end=0.9))
fullhits4 = (statecounts4['1',])
#fullhits4 = statecounts4['1',]+statecounts4['2',]
maybehits4 = (statecounts4['1',]+statecounts4['2',]+statecounts4['8',]+statecounts4['32',]+statecounts4['64',])
allhits4 = (colSums(statecounts4[1:8,]))
nohits4 = (statecounts4['256',])
summap4 = datmap4[,-1]+datmap4[,-ncol(datmap4)]
diffmap4 = t(diff(t(datmap4)))
fdmap4 = diffmap4
fdmap4[fdmap4<1]=(-1)
signmap4 = (-1)*sign(fdmap4)
modmap4 = summap4*signmap4
finalmap4 = matrix(append(datmap4[,1]+max(k),modmap4), nrow=248,ncol=colcount4)
mapcounts4 = apply(finalmap4,2,function(x) table(factor(x,levels=sort(mc))))
maybetomaybe4 = (mapcounts4['4',]+mapcounts4['128',])
sametosame4 = mapcounts4['2',]+mapcounts4['4',]+mapcounts4['16',]+mapcounts4['64',]+mapcounts4['128',]
anytofull4 = (mapcounts4['2',]+mapcounts4['3',]+mapcounts4['5',]+mapcounts4['9',]+mapcounts4['17',]+mapcounts4['33',]+mapcounts4['65',]+mapcounts4['129',]+mapcounts4['257',])

data5 = read.table(file5, header=FALSE, sep="\n")
colcount5 = 9
datmap5 = matrix(data5[,1],nrow=248,ncol=colcount5)
datmap5 = subset(datmap5, select=c(2,1,3,4,5,6,7,8,9))
statecounts5 = apply(datmap5,2,function(x) table(factor(x, levels=k)))
fullhits5 = (statecounts5['1',])
#fullhits5 = statecounts5['1',]+statecounts5['2',]
maybehits5 = (statecounts5['1',]+statecounts5['2',]+statecounts5['8',]+statecounts5['32',]+statecounts5['64',])
allhits5 = (colSums(statecounts5[1:8,]))
nohits5 = (statecounts5['256',])

data6 = read.table(file6, header=FALSE, sep="\n")
colcount6 = 6
datmap6 = matrix(data6[,1],nrow=248,ncol=colcount6)
#datmap6 = subset(datmap6, select=c(1,2,3,4,5,6))
statecounts6 = apply(datmap6,2,function(x) table(factor(x, levels=k)))
fullhits6 = (statecounts6['1',])
#fullhits6 = statecounts6['1',]+statecounts6['2',]
maybehits6 = (statecounts6['1',]+statecounts6['2',]+statecounts6['8',]+statecounts6['32',]+statecounts6['64',])
allhits6 = (colSums(statecounts6[1:8,]))
nohits6 = (statecounts6['256',])

data9 = read.table(file9, header=FALSE, sep="\n")
colcount9 = 6
datmap9 = matrix(data9[,1],nrow=248,ncol=colcount9)
statecounts9 = apply(datmap9,2,function(x) table(factor(x, levels=k)))
fullhits9 = (statecounts9['1',])
allhits9 = (colSums(statecounts9[1:8,]))

x1 = c(1,5,10,20,30,40,50,60,70,80)
x2 = c(1,5,10,20,30,57)
x3 = c(1,1.5,2,5,10,20,30,40,64)
x9 = c(1,5,10,20,30,43)
#x4 = c(1,10,20,30,38)
x4 = c(1,10,20,30,40,50,60,70)
x5 = c(1,10,20,30,40,50,60,70,80)
x6 = c(1,10,20,30,40,56)
ymin = 0
ymax = 250
mycolors=rep(c("black"),19)
plotchars=c(1,2,5,6,13,22,16)
linewid = 2
cexsize = 2
par(mfrow=c(1,2), mar=c(5,4,2,1))
ytext1 = "Number of Conserved Eukaryotic Genes"
plot(x1,allhits1, xlim=range(x1), ylim=range(0,250), frame.plot=FALSE, xlab="Reads (Millions)", ylab=ytext1, type='b', col=mycolors[18], pch=plotchars[1], cex=cexsize, lwd=linewid, lty=2)
lines(x2,allhits2,type='b',col=mycolors[14], pch=plotchars[2], cex=cexsize, lwd=linewid, lty=2)
lines(x3,allhits3,type='b',col=mycolors[10], pch=plotchars[3], cex=cexsize, lwd=linewid, lty=2)
lines(x1,fullhits1,type='b',col=mycolors[18], pch=plotchars[1], cex=cexsize, lwd=linewid, lty=1)
lines(x2,fullhits2,type='b',col=mycolors[14], pch=plotchars[2], cex=cexsize, lwd=linewid, lty=1)
lines(x3,fullhits3,type='b',col=mycolors[10], pch=plotchars[3], cex=cexsize, lwd=linewid, lty=1)
#lines(x9,allhits9,type='b',col=mycolors[2], pch=plotchars[1], cex=cexsize, lwd=linewid)
#lines(x9,fullhits9,type='b',col=mycolors[2], pch=plotchars[2], cex=cexsize, lwd=linewid)
#lines(x5,allhits5,type='b',col=mycolors[1], pch=plotchars[1], cex=cexsize, lwd=linewid)
#lines(x5,maybehits5,type='b',col=mycolors[1], pch=plotchars[3], cex=cexsize, lwd=linewid)
legend(50,100,legend=c("C.multidentata","H.californensis","P.robusta"), text.col=mycolors[c(18,14,10)], pch=plotchars[c(1,2,3)], pt.cex=1.5, col=mycolors[c(18,14,10)], title="Samples", title.col="black")
mtext("A",adj=0, cex=1.5, line=-0.1)
par(mar=c(5,2,2,1))
plot(x5,allhits5, xlim=range(x5), ylim=range(0,250), frame.plot=FALSE, xlab="Reads (Millions)", ylab="", type='b', col=mycolors[1], pch=plotchars[6], cex=cexsize, lwd=linewid, lty=2)
lines(x4,allhits4,type='b',col=mycolors[3], pch=plotchars[5], cex=cexsize, lwd=linewid, lty=2)
#lines(x4,fullhits4+maybetomaybe4,type='b',col=mycolors[3], pch=plotchars[3], cex=cexsize, lwd=linewid)
lines(x6,allhits6,type='b',col=mycolors[8], pch=plotchars[4], cex=cexsize, lwd=linewid, lty=2)
lines(x4,fullhits4,type='b',col=mycolors[3], pch=plotchars[5], cex=cexsize, lwd=linewid, lty=1)
lines(x5,fullhits5,type='b',col=mycolors[1], pch=plotchars[6], cex=cexsize, lwd=linewid, lty=1)
lines(x6,fullhits6,type='b',col=mycolors[8], pch=plotchars[4], cex=cexsize, lwd=linewid, lty=1)
legend(58,100,legend=c("D.gigas","H.imbricata","S.similis","Mouse C10"), text.col=c(mycolors[c(8,3,1)],"black"), pch=plotchars[c(4,5,6,7)], pt.cex=1.5, col=c(mycolors[c(8,3,1)],"black"), title="Samples", title.col="black")
mtext("B",adj=0, cex=1.5, line=-0.1)

lines(xmouse, fullseqs_c10, type='b', col="black", pch=plotchars[7], cex=1.75, lwd=linewid, lty=2)
lines(xmouse, fullhits_c10, type='b', col="black", pch=plotchars[7], cex=cexsize, lwd=linewid, lty=1)

#plot(otherreads[1:6], othercounts[1:6], axes=FALSE, xlab="", ylab="", xlim=range(x), ylim=c(ymin,ymax), type='p', col=mycolors[1:6], pch=plotchars[1:6], cex=1.2)
#par(new=TRUE)
#plot(x[1:4], Tomo, axes=FALSE, xlab="", ylab="", xlim=range(x), ylim=c(ymin,ymax), type='b', col=mycolors[7], pch=plotchars[7], cex=1.2)
#par(new=TRUE)
#plot(x,Cmulti, xlab="Number of reads (millions)", axes=FALSE, xlim=range(x), ylim=c(ymin,ymax), ylab="", type='b', col=mycolors[8], pch=plotchars[8], main="", cex=1.2)
#axis(2, ylim=c(ymin,ymax))
#axis(1, pretty(range(x)))
#mtext("Number of COGs",side=2, line=2)

#lines(x, SNL, type='b', col=mycolors[9], pch=plotchars[9], cex=1.2)

#legend(55,100,legend=c("C.multidentata","H.californensis","P.robusta","D.gigas","T.nisseni","S.similis"), text.col=mycolors[c(18,14,10,8,3,1)], pch=plotchars[1], pt.cex=1.5, col=mycolors[c(18,14,10,8,3,1)], title="Samples", title.col="black")

#dev.off()