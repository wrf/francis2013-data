file1 = "~/cogdata/Core_genes_Parra.sizes.txt"
file2 = "~/cogdata/opt-seq-trans-mouse_data/mouse_Parra_proteins.sizes.txt"
file3 = "~/assembly/opt-seq-trans_data/mouse_heart_SRR453174_all_prot_kogs.txt"
#
outfilename = "/home/dummy/Dropbox/thesis_final/osd_fig_6.pdf"
pdf(file=outfilename, height = 6, width = 10)
#

data1 = read.table(file1, header=FALSE)
sizes1 = data1[,1]
sizemap1 = matrix(sizes1, byrow=TRUE, ncol=6)
mins = apply(sizemap1, 1, min)
maxes = apply(sizemap1, 1, max)

data2 = read.table(file2, header=FALSE)

data3 = read.table(file3, header=FALSE)
datmap3 = matrix(data3[,1], nrow=248)
datmap3 = subset(datmap3, select=c(2,7,1,3,4,5,6,8,9))
finalmap = matrix(c(datmap3,mins,maxes,data2[,1]), nrow=248)


par(mfrow=c(2,3), mar=c(4.5,4.5,2,1), las=0)

xmouse = c(1,5,10,20,30,40,50,60,70)
col1 = "blue" # not used
col2 = "red" # not used
col3 = "black"
#col3 = "#CC9944" #alt color
col4 = "#444444"
col5 = "white"
#col5 = "#22BBBB" #alt color
linewidth = 1.5
labelsize = 1.2
xlabel = "Reads (Millions)"
ylabel = "Protein length (Amino Acids)"
k2 = 17 #alanyl trna snythetase
plot(range(xmouse),rep(finalmap[k2,12],2),col=col3, type='l', lwd=2, lty=3, cex.lab=labelsize, xlim=c(0,70), ylim=range(pretty(0:finalmap[k2,12]+20)), frame.plot=FALSE, xlab=xlabel, ylab=ylabel, main="AlaRS")
lines(xmouse, finalmap[k2,1:9], type='b', pch=19, cex=1.5, lwd=linewidth)
points(c(30),c(907),col=col5, pch=19, cex=1.4) # single point at 30M to show the red dot of the chimeric bar
#lines(range(xmouse),rep(finalmap[k2,11],2),col=col2)
#lines(range(xmouse),rep(finalmap[k2,10],2),col=col1)
mtext("A",adj=0, cex=1.5, line=-0.1)
par(mar=c(4.5,3,2,1))
k3 = 52 #2 oxoglutarate dehydrogenase e2
plot(range(xmouse),rep(finalmap[k3,12],2),col=col3, type='l', lwd=2, lty=3, cex.lab=labelsize, xlim=c(0,70), ylim=range(pretty(0:finalmap[k3,12]+20)), frame.plot=FALSE, xlab=xlabel, ylab="", main="2OGDH-E2" )
lines(xmouse, finalmap[k3,1:9], type='b', pch=19, cex=1.5, lwd=linewidth)
#lines(range(xmouse),rep(finalmap[k3,11],2),col=col2)
#lines(range(xmouse),rep(finalmap[k3,10],2),col=col1)
k1 = 47 #elongation factor 2
plot(range(xmouse),rep(finalmap[k1,12],2),col=col3, type='l', lwd=2, lty=3, cex.lab=labelsize, xlim=c(0,70), ylim=range(pretty(0:finalmap[k1,12]+20)), frame.plot=FALSE, xlab=xlabel, ylab="", main="EF2")
lines(xmouse, finalmap[k1,1:9], type='b', pch=19, cex=1.5, lwd=linewidth )
#lines(range(xmouse),rep(finalmap[k1,11],2),col=col2)
#lines(range(xmouse),rep(finalmap[k1,10],2),col=col1)

k4 = 161
k5 = 96
k6 = 219 # chmp1b1

par(las=2, mar=c(4.5,4.5,2,1))
barnames = rev(c("Ref","1","5","10","20","30","40","50","60","70"))
bars1 = c(0,0,0,0,968,0,0,0,0,0,607,134,0,0,0,99,298,571,0,0,0,397,571,0,0,45,198,10,709,0,0,761,207,0,0,0,968,0,0,0,0,968,0,0,0,0,968,0,0,0)
barmatrix1 = subset(t(matrix(bars1,nrow=10,byrow=TRUE)), select=c(10:1))
barplot(barmatrix1,space=1.2, cex.lab=labelsize,names.arg=barnames,horiz=TRUE,xlim=c(0,1000), col=c("white",col4,"white",col4,col3), density=c(-1,-1,-1,30,-1), border=NA,xlab=ylabel, ylab=xlabel)

par(las=0)
mtext("B",adj=0, cex=1.5, line=-0.1)

par(las=2, mar=c(4.5,3,2,1))
bars2 = c(28,0,454,185,171,0,28,454,0,75,407,0,28,454,0,0,482,0,28,454,0,0,482,0,0,303,0,28,454,0)
barmatrix2 = subset(t(matrix(bars2,nrow=10,byrow=TRUE)), select=c(10:1))
barplot(barmatrix2,space=1.2,cex.lab=labelsize,names.arg=barnames,horiz=TRUE,xlim=c(-28,480),offset=-28, col=c("white",col4,col3),border=NA,xlab=ylabel, ylab="")
bars3 = c(0,0,0,0,858,0,858,0,0,0,0,858,0,0,0,122,736,0,0,0,34,814,0,0,0,155,156,6,541,0,339,519,0,0,0,155,156,6,541,0,603,255,0,0,0,155,155,6,342,0)
#gaps are exaggerated by 5 for display
barmatrix3 = subset(t(matrix(bars3,nrow=10,byrow=TRUE)), select=c(10:1))
barplot(barmatrix3,space=1.2,cex.lab=labelsize,names.arg=barnames,horiz=TRUE,xlim=c(0,900),col=c("white",col4,"white",col4,col3),border=NA,xlab=ylabel, ylab="")
par(las=0)

dev.off()
