Filename	Sequences	Total	Mean	Median	n50	n90
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_h01_transcripts.fa	4268	3476633	814	622	1173	2693
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_h05_transcripts.fa	25532	24797254	971	690	1443	3873
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_h10_transcripts.fa	51525	57235717	1110	806	1674	4256
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_h20_transcripts.fa	88208	118020351	1337	1007	2031	4655
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_h30_transcripts.fa	121439	176637848	1454	1097	2193	4980
/home/biolum/ncbi-blast-2.2.25+/db/hormiphora_transcripts.fa	175701	272229858	1549	1153	2373	5556
