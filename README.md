# Data from #

Francis, W.R., et al (2013) [A comparison across non-model animals suggests an optimal sequencing depth for de novo transcriptome assembly](https://www.ncbi.nlm.nih.gov/pubmed/23496952). **BMC Genomics** 14:167

![francis2013_fig_4.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/francis2013_fig_4.png)

## Files ##


## Samples ##
Generated in this study:

* Photophores of the Humboldt squid [Dosidicus gigas](https://bitbucket.org/wrf/squid-transcriptomes/downloads/dosidicus_gigas_trinity_norm.fasta.gz), 60M reads at [SRR5152122](https://www.ncbi.nlm.nih.gov/sra/SRX2464239)
* Elytra of the scaleworm [Harmothoe imbricata](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/harmothoe_imbricata_trinity.fasta.gz), 75M reads at [SRR4841788](https://www.ncbi.nlm.nih.gov/sra/SRX2321756)
* Whole body of the ctenophore [Hormiphora californensis](https://bitbucket.org/wrf/ctenophore-transcriptomes/src), 64M reads at [SRR1992642](https://www.ncbi.nlm.nih.gov/sra/SRX1006956)
* Multiple whole individuals of the copepod [Pleuromamma robusta](https://bitbucket.org/wrf/francis2013-data/downloads/pleuromamma_trinity.renamed.fasta.gz), 64M reads at [SRX3341162](https://www.ncbi.nlm.nih.gov/sra/SRX3341162)
* Whole individual of the siphonophore Chuniphyes multidentata, 103M reads at [SRR6232790](https://www.ncbi.nlm.nih.gov/sra/SRX3341168) and [SRR6232791](https://www.ncbi.nlm.nih.gov/sra/SRX3341169)
* Legs/abdomen of the decapod [Sergestes similis](https://bitbucket.org/wrf/francis2013-data/downloads/sergestes_trinity.renamed.fasta.gz), 93M reads at [SRR6232730](https://www.ncbi.nlm.nih.gov/sra/SRX3341160)

Public data:

* Heart tissue of the mouse, 82M reads at [SRR453174](https://www.ncbi.nlm.nih.gov/sra/SRX135166)
